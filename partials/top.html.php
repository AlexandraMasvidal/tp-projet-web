<?php require_once 'code-behind/menu.php' ?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/reset.css">
    <title><?php echo $page_title ?></title>
</head>
<body>

    <header class="container">
        <a class="logo" href="index.php">
            <img src="../media/logo.png" alt="logo toys-r-us">
        </a>
        <nav class="nav-menu">
            <ul>
                <li><a href="liste.php" class="main-nav-link">Tous les jouets</a></li>
                <li>
                    <a href="liste.php"  class="main-nav-link dropdown">Par marque</a>
                    <ul class="sous-menu-marques">
                        <?php getAllBrandsMenu($result_brands) ?>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>

    <main class="container">
        <h1 class="page-title"> <?php echo $page_title ?> </h1>


