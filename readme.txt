TP Projet Site Web Toy'R'Us

Int�grer exactement comme sur la maquette.

La BDD contient:
- "brands": Marques
- "sales": Ventes par jouet et par date
- "stock": Stock disponible par jouet et par magasin
- "stores": Magasin
- "toys": Jouets (Nom, description, id marque, prix, nom du fichier de l'image)

Le site se compose de 3 pages:

OK- Accueil: Top 3 des meilleures ventes de l'ann�e en cours.
OK- Liste: Liste des jouets avec filtre par marque.
- D�tail: D�tail d'un jouet particulier. Attention Marque dans une autre table. Stock : jointure � faire + filtre qui fonctionne comme pour la liste avec un filtre dans l'url pour afficher le stock par magasin => aggr�gation � faire pour avoir le stock de tous les magasins

Images des jouets:
A stocker dans un dossier d�di� (nom au choix).

Menus:
OK- "Tous les jouets": Lien vers liste compl�te.
OK- "Par marque": Sous-menu construit � partir de la BDD avec liens vers la page de liste pr�filtr�e sur la marque choisie. (avec GET)

D�tail:
- Information basiques du jouet
- Stock total, avec possibilit� de choisir un magasin pour affiner ce chiffre.


BONUS (� faire si tout est termin�):
- Responsive pour mobile (� imaginer)
- Dans le sous-menu, afficher le nombre de jouets diff�rents pour chaque marque (ex: "Mattel (6)")
- Dans la liste:
  - Tri par prix croissant/d�croissant
  - Pagination (4 jouets par page)
  
  NB : faire des essais de requ�tes dans le client sql
  
  Background : la colonne du milieu doit rester � taille fixe au milieu
  
  
  requ�te pour afficher le nombre de jouets par magasin
  
  SELECT
    s.id,
    s.name,
    SUM(stock.quantity) as total_toys,
    stock.toy_id,
    toys.name
    FROM stores s
    JOIN stock ON stock.store_id = s.id
    JOIN toys ON  toys.id = stock.toy_id
    where toys.id = 1 AND s.id = 2
    GROUP BY s.id
;
  
  
