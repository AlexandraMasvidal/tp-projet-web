<?php require_once './code-behind/code-detail.php' ;
require_once  './partials/top.html.php'
?>

<div class="detail row">
    <div class="col left">
        <div class="img">
            <img src="media/<?php echo $image ?>" alt="<?php echo $title ?>">
        </div>
        <div class="price">
            <?php echo $price ?>€
        </div>

        <div class="stock">
            <div class="stock-choice">
                <form action="" method="get">
                    <select name="magasin" id="">
                        <option value="">Quel magasin ?</option>
                        <?php getAllStores() ?>
                        <input type="hidden" name="id" value="<?php echo $id_toy  ?>">

                    </select>
                    <input type="submit" value="Ok">
                </form>
                <div class="display-stock">
                    <span class="blue-key">Stock: </span><span class="black-value"> <?php echo $stock ?> </span>
                </div>
            </div>

        </div>

    </div>
    <div class="col right">
        <div class="marque">
            <span class="blue-key">Marque: </span> <span class="black-value"><?php echo $marque ?></span>
        </div>
        <div class="description">
            <?php echo $description ?>
        </div>
    </div>
</div>

<?php require_once  './partials/bottom.html.php' ?>
