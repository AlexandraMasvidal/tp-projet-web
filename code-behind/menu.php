<?php

/*requête pour avoir l'ensemble des marques */
$result_brands= [];
$q_brands = 'SELECT 
                    b.name, b.id,
                    count(toys.brand_id) as toys
                FROM brands as b
                JOIN toys on toys.brand_id = b.id
                group by toys.brand_id
                order by b.name';
$q_brands_result = mysqli_query($mysqli, $q_brands);
if($q_brands_result) {
    while($brand = mysqli_fetch_assoc($q_brands_result)) {
        $result_brands[] = $brand;
    }
}

/*fonction pour afficher l'ensemble des marques dans le sous menu */
function getAllBrandsMenu($result_brands) {
    foreach($result_brands as $brand) {
        echo '<li><a href="liste.php?id=' . $brand['id'] . '">' . $brand['name'] . ' (' . $brand['toys'] . ')' . '</a></li>';
    }
}
