<?php require_once '_init.php';

//Variables :
    /*tunnel vers BDD*/
    $mysqli = mysqli_connect(DB_HOST, DB_NAME, DB_PASS, DB_USER);
    /*Initialisation de la requête mysql qui changera en fonction de la demande de l'utilisateur (choix d'un magasin ou non)*/
    $q_prep = '';
    /*Tableau regroupant les arguments à passer au stmt bind param*/
    $stmt_bind_param_vars = [];
    $type = '';
    /*Initialisation du tableau de résultats de la requête*/
    $result = [];
    $toy = '';


/*FONCTION QUI GENERE LES MARQUES A AFFICHER DANS LE MENU DEROULANT **************************************/

function getAllStores() : void
{
    global $mysqli;
    /*requête pour avoir l'ensemble des marques */
    $q = 'SELECT id, city 
        FROM stores order by name;';
    $q_list_cities_result = mysqli_query($mysqli, $q);

    if ($q_list_cities_result) {
        while ($city = mysqli_fetch_assoc($q_list_cities_result)){

            if(empty($_GET['magasin'])) {
                echo '<option value="' . $city['id'] . '">' . $city['city'] . '</option>';
            }
            if(!empty($_GET['magasin'])) {
                if($_GET['magasin'] === $city['id']) {
                    echo '<option selected value="' . $city['id'] . '">' . $city['city'] . '</option>';
                } else {
                    echo '<option value="' . $city['id'] . '">' . $city['city'] . '</option>';
                }
            }
        }
    }
}


/*AFFICHAGE DU STOCK*************************************************************************************/

/*Si la variable globale get existe et contient l'id du jouet (envoyé par un clic depuis la liste des jouets ou de la page d'accueil)*/
if(!empty($_GET) && !empty($_GET['id'])) {

    /*On stocke l'id du jouet dans une variable que l'on push dans le tableau d'arguments pour la requête*/
    $id_toy = $_GET['id'];
    $stmt_bind_param_vars[] = $id_toy;
    $type .= 'i';

    /*Si le Get ne contient pas de clef magasin on définit la requête sql pour afficher le stock de tous les magasins pour le jouet*/
    if(empty($_GET['magasin'])) {
        $q_prep = 'SELECT
                        SUM(s.quantity) as total_toys, s.toy_id,
                        toys.id, toys.name, toys.price, toys.description, toys.brand_id, toys.image,
                        brands.name as b_name
                    FROM stock s
                    JOIN toys on toys.id = s.toy_id
                    JOIN brands on brands.id = toys.brand_id
                    WHERE s.toy_id = ?
                    GROUP BY s.toy_id;';
    }

    /*Si le get contient la clef magasin :
      on récupère la valeur de la clef magasin dans une variable
      on la push dans le tableau d'arguments pour la requête et
      on créé la requête pour récupérer le stock du magasin choisi*/
    if(!empty($_GET['magasin'])) {
        $id_magasin = $_GET['magasin'];
        $stmt_bind_param_vars[] = $id_magasin;
        $type .= 'i';
        $q_prep = 'SELECT
                        s.quantity as total_toys, s.toy_id,
                        stores.id, stores.city,
                        toys.id, toys.name, toys.price, toys.description, toys.brand_id, toys.image,
                        brands.name as b_name
                            FROM stock s
                             JOIN stores ON s.store_id = stores.id
                             JOIN toys ON toys.id = s.toy_id
                             JOIN brands on brands.id = toys.brand_id
                             WHERE toys.id=? AND store_id=?';
    }

    /*On lance la requête avec la préparation via le tableau d'arguments qui contiendra une ou deux variables
     suivant le résultat du if ci-dessus et on récupère le résultat dans le tableau result*/
    if($stmt = mysqli_prepare($mysqli, $q_prep)) {
        if(mysqli_stmt_bind_param($stmt, $type, ...$stmt_bind_param_vars)) {
            mysqli_stmt_execute($stmt);
            $q_result= mysqli_stmt_get_result($stmt);
            mysqli_stmt_close($stmt);

            if($q_result) {
                $toy = mysqli_fetch_assoc($q_result);
            }
        }
    }

    /*On stocke les infos dans des variables qu'on appelle directement dans details.php pour les afficher*/
    $stock = $toy['total_toys'];
    $image = $toy['image'];
    $title = $toy['name'];
    $price = $toy['price'];
    $marque = $toy['b_name'];
    $description = $toy['description'];
    $page_title = $title;

}
else {
    $page_title = 'NOM DU JOUET.html';
}



?>