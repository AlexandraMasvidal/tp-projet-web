<?php require_once '_init.php';


$page_title = 'Les Jouets';
$mysqli = mysqli_connect(DB_HOST, DB_NAME, DB_PASS, DB_USER);

/**** AFFICHAGE DU MENU SELECT DES MARQUES ****/

function getAllBrands() : void
{
    global $mysqli;
    /*$q_brands = 'SELECT name, id FROM lamp.brands order by name ;';*/
    $q_brands = 'SELECT 
                    b.name, b.id,
                    count(toys.brand_id) as toys
                FROM brands as b
                JOIN toys on toys.brand_id = b.id
                group by toys.brand_id
                order by b.name' ;
    $q_brands_result = mysqli_query($mysqli, $q_brands);

    if($q_brands_result) {
        while($brand = mysqli_fetch_assoc($q_brands_result)) {
            /*Si aucune marque n'est sélectionnée dans le menu select, on affiche tous les produits*/
            if(empty($_GET['id'])) {
                echo'lol';
                echo '<option value="'. $brand['id'] . '">' . $brand['name'] . ' (' . $brand['toys'] . ')' . '</option>';
            }
            /*Si une marque est sélectionnée on affiche la liste filtrée et l'option de la marque reste selectionnée */
            if(!empty($_GET['id'])) {
                if($_GET['id'] === $brand['id']) {
                    echo '<option selected value="'. $brand['id'] . '">' . $brand['name'] . ' (' . $brand['toys'] . ')' . '</option>';
                } else {
                    echo '<option value="'. $brand['id'] . '">' . $brand['name'] . ' (' . $brand['toys'] . ')' . '</option>';
                }
            }
        }
    }
}


/**** AFFICHAGE DE LA LISTE DES JOUETS ****/

    $result= [];
    $q = '';
    /*requête pour avoir l'ensemble des jouets*/
    if(empty($_GET['id'])) {
        $id_brand = '';
        $q = 'SELECT name, price, image, id FROM lamp.toys;';
        $q_result = mysqli_query($mysqli, $q);
        if($q_result) {
            while($toy = mysqli_fetch_assoc($q_result)) {
                $result[] = $toy;
            }
        }
    } else {
    /*requête pour avoir les jouets de la marque sélectionnée */
        $id_brand = $_GET['id'];
        $q = 'SELECT t.name, t.price, t.image, t.id, brand_id, brands.name 
            FROM lamp.toys t
            JOIN brands ON brands.id = t.brand_id
            WHERE brands.id = ?;';
        if($stmt = mysqli_prepare($mysqli, $q)) {
            if(mysqli_stmt_bind_param($stmt, 'i', $id_brand)) {
                mysqli_stmt_execute($stmt);
                $result_by_brand = mysqli_stmt_get_result($stmt);
                mysqli_stmt_close($stmt);
                if($result_by_brand) {
                    while ($toy = mysqli_fetch_assoc($result_by_brand)) {
                        $result[] = $toy;
                    }
                }
            }
        }
    }

    /*Fonction pour afficher les jouets*/
    function getToys ($result) {
        foreach($result as $toy) {
            echo '<a class="item" href="details.php?id=' . $toy['id'] .'">';
            echo    '<div class="frame">';
            echo        '<img src="media/' . $toy['image'] . '" alt="img">';
            echo    '</div>';
            echo    '<div class="item-title">';
            echo        $toy['name'];
            echo     '</div>';
            echo    '<div class="item-price">';
            echo        $toy['price'] . ' €';
            echo     '</div>';
            echo  '</a>';

        }
    }




?>