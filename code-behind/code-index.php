<?php require_once '_init.php';
$page_title = 'N°1 des ventes il ya très longtemps dans une galaxie lointaine';

$mysqli = mysqli_connect(DB_HOST, DB_NAME, DB_PASS, DB_USER);
$result= [];
$q = '
    SELECT t.id,  t.name, SUM(t.price) as price, t.image, SUM(sales.quantity) AS Q
        FROM toys t
        JOIN sales ON toy_id = t.id
        GROUP BY(t.id)
        ORDER BY Q desc, price desc
        limit 3';
$q_result = mysqli_query($mysqli, $q);
if($q_result) {
    while($toy = mysqli_fetch_assoc($q_result)) {
        $result[] = $toy;
    }
}


/*Fonction pour afficher le top 3*/
function getBestSales ($result) {
    foreach($result as $toy) {
        echo '<a class="item" href="details.php?id=' . $toy['id'] .'">';
        echo    '<div class="frame">';
        echo        '<img src="media/' . $toy['image'] . '" alt="img">';
        echo    '</div>';
        echo    '<div class="item-title">';
        echo        $toy['name'];
        echo    '</div>';
        echo    '<div class="item-price">';
        echo        $toy['price'] . ' €';
        echo     '</div>';
        echo  '</a>';

    }
}
?>