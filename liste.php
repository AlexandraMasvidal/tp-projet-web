<?php require_once './code-behind/code-list.php' ;
require_once  './partials/top.html.php'
?>

<form action="" method="GET">
    <select name="id">
        <option value="" selected>Quelle marque ?</option>
        <?php getAllBrands() ?>
    </select>
    <input type="submit" value="Ok">
</form>


<div class="item-list">
    <?php getToys($result) ?>
</div>

<?php require_once  './partials/bottom.html.php' ?>
